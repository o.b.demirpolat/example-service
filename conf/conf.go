package conf

type Config struct {
	SomeApplicationConfig string
}

func Init() *Config {
	return &Config{SomeApplicationConfig: "test"}
}
