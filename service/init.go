package service

import (
	"example-service/client"
	"example-service/conf"
	"go.uber.org/zap"
)

type Services struct {
	HomePage HomePageService
}

func Init(clients *client.Clients, config *conf.Config, logger *zap.SugaredLogger) *Services {
	services := &Services{}
	services.HomePage = HomePageService{
		clients: clients,
		logger:  logger,
		config:  config,
	}
	return services
}
