package service

import (
	"context"
	client2 "example-service/client"
	"example-service/conf"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type HomePageService struct {
	clients *client2.Clients
	logger  *zap.SugaredLogger
	config  *conf.Config
}

type HomePageReq struct {
	HasOrder bool `json:"has_order"`
}

type HomePageRes struct {
	HomePageWidget interface{}
}

// HomePageServiceFunc gets data from orange and another backend services then serves widgets and data for homepage screen.
func (h HomePageService) HomePageServiceFunc(req HomePageReq) (*HomePageRes, error) {
	getTokenReq := client2.OrangeGetTokenReq{}
	_, err := h.clients.Orange.GetToken(context.Background(), getTokenReq)
	if err != nil {
		err = errors.Wrap(err, "error occured while getting token from orange")
		h.logger.Warn(err)
		return nil, err
	}
	response := &HomePageRes{HomePageWidget: nil}
	return response, nil
}
