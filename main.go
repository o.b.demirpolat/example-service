package main

import (
	"example-service/client"
	"example-service/conf"
	"example-service/handler"
	"example-service/log"
	"example-service/service"
	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	config := conf.Init()
	logger := log.Init()

	clients := client.Init(config, logger)
	services := service.Init(clients, config, logger)
	handlers := handler.Init(services, config, logger)

	app.Get("/homepage", handlers.HomePageHandler.GetHomePage)
	err := app.Listen(":3000")
	if err != nil {
		logger.Panic(err)
	}
}
