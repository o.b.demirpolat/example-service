package client

import (
	"context"
	"example-service/conf"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"net/http"
)

type OrangeClient struct {
	Config     *conf.Config
	Logger     *zap.SugaredLogger
	HttpClient *http.Client
}

type OrangeGetTokenReq struct {
	Name string
}

type OrangeGetTokenRes struct {
	Code int
}

func (o OrangeClient) GetToken(c context.Context, req OrangeGetTokenReq) (*OrangeGetTokenRes, error) {
	return nil, errors.New("error occured in GetToken, orange service status code: 500")
}
