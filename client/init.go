package client

import (
	"example-service/conf"
	"go.uber.org/zap"
	"net/http"
)

func init() {}

type Clients struct {
	Orange OrangeClient
}

func Init(config *conf.Config, logger *zap.SugaredLogger) *Clients {
	clients := &Clients{}
	clients.Orange = OrangeClient{
		Config:     config,
		Logger:     logger,
		HttpClient: &http.Client{},
	}

	return clients
}
