package handler

type Response struct {
	Error string
	Data  interface{}
}
