package handler

import (
	"example-service/conf"
	"example-service/service"
	"go.uber.org/zap"
)

type Handler struct {
	HomePageHandler HomePageHandler
}

func Init(services *service.Services, config *conf.Config, logger *zap.SugaredLogger) Handler {
	handler := Handler{}
	handler.HomePageHandler = HomePageHandler{
		Services: services,
		Config:   config,
		Logger:   logger,
	}
	return handler
}
