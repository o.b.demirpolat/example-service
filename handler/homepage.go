package handler

import (
	"example-service/conf"
	"example-service/service"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type HomePageHandler struct {
	Services *service.Services
	Config   *conf.Config
	Logger   *zap.SugaredLogger
}

func (h HomePageHandler) GetHomePage(c *fiber.Ctx) error {
	res, err := h.Services.HomePage.HomePageServiceFunc(service.HomePageReq{HasOrder: false})
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(Response{Error: err.Error()})
	}

	return c.Status(fiber.StatusOK).JSON(Response{Data: res})
}
